from django.db import models

# Create your models here.


class IPAddress(models.Model):
    #id = models.AutoField(primary_key=True)
    IP = models.GenericIPAddressField(
    unique=True,
    protocol='both', unpack_ipv4=False,
        help_text='IPv4 or IPv6 address')
    first_seen = models.DateTimeField(auto_now_add=True,
        help_text='Timestamp this IP address was observed first')
    last_seen = models.DateTimeField(auto_now=True,
        #initial=datetime.datetime.utcnow,
        help_text='Timestamp this IP address was observed for the last time')

    def __str__(self):
        friendlyname = str(self.IP)
        return friendlyname

    class Meta:
        verbose_name='IP address'
        verbose_name_plural='IP addresses'
